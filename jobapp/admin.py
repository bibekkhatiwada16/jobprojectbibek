from django.contrib import admin

# Register your models here.

from .models import *

admin.site.register([Admin, Employer, JobSeeker,
                     JobCatagory, Job, JobApplication])
