from django import forms
from .models import *


class JobSeekerForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = JobSeeker  # model name
        # fields = '__all__'
        # exclude = ['user']
        fields = ['username', 'password', 'confirm_password', 'name', 'address',
                  'mobile', 'image', 'qualification', 'skills', 'about', 'cv']

    def clean_username(self):
        username = self.cleaned_data.get('username')

        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "Jobseeker with this username already exists"


            )
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        c_password = self.cleaned_data.get('confirm_password')

        if password != c_password:
            raise forms.ValidationError(

                'Password Doesnot Match'


            )

        return c_password


# model form to save data automatically #this form layout gets  render at jobseekerjobapply.html
class JobApplyForm(forms.ModelForm):
    class Meta:
        model = JobApplication
        fields = ['cover_letter']


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


# FOR EMPLOYER SECTION

class EmployerForm(forms.ModelForm):

    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Employer
        fields = ['username', 'password', 'confirm_password', 'name', 'mobile', 'email', 'address',
                  'image', 'company', 'website', 'company_image']

    # to validate the Employer User

    def clean_username(self):
        username = self.cleaned_data.get(
            'username')  # form bata haleko user get garne

        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError(
                "Employer with this username already exists"


            )
        return username

    def clean_confirm_password(self):
        password = self.cleaned_data.get('password')
        c_password = self.cleaned_data.get('confirm_password')

        if password != c_password:
            raise forms.ValidationError(

                'Password Doesnot Match'


            )

        return c_password


# form to post job by employer
class EmployerJobPostForm(forms.ModelForm):
    class Meta:
        model = Job
        # we dont need employer to select himdelf to create job]
        exclude = ['employer']
        widgets = {
            'title': forms.TextInput(attrs={
                'placeholder': 'Enter Job Title Here',
                'id': 'jobtitle'
            }),
            'catagory': forms.Select(attrs={
                'id': 'jobcatagory',
                'class': 'form-control'
            }),


        }
