from django.urls import path
from .views import *

app_name = "jobapp"

urlpatterns = [

    path('', JobSeekerHomeView.as_view(), name='jobseekerhome'),
    path('jobseeker/registration/', JobSeekerRegistrationView.as_view(),
         name='jobseekerregistration'),
    path('job/list/', JobListView.as_view(), name='joblist'),
    path('job/<int:pk>/detail/', JobDetailView.as_view(),
         name='jobdetail'),  # primary key is mandatory for detail
    path('job/<int:pk>/apply/', JobSeekerJobApplyView.as_view(),
         name='jobseekerjobapply'),  # primary key to take job

    path('login/', LoginView.as_view(), name='login'),
    path('jobseeker/profile/', JobSeekerProfileView.as_view(),
         name='jobseekerprofile'),




    # for employer section

    path('employer/registration/', EmployerRegistrationView.as_view(),
         name='employerregistration'),
    path('employer/home/', EmployerHomeView.as_view(), name='employerhome'),
    path('employer/profile/',
         EmployerProfileView.as_view(), name='employerprofile'),

    path('employer/job/<int:pk>/detail/',
         EmployerJobDetailView.as_view(), name='employerjobdetail'),\

    path('employer/job/post/', EmployerJobCreateView.as_view(),
         name='employerjobcreate'),


]
