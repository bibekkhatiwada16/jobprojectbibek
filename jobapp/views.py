from django.shortcuts import render, redirect
from django.views.generic import *
from .models import *
from .forms import *
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import Group
from django.urls import reverse, reverse_lazy


class EmployerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        usr = request.user
        if usr.is_authenticated and usr.groups.filter(name='employer').exists():
            pass
        else:
            return redirect('jobapp:login')

        return super().dispatch(request, *args, **kwargs)


# Create your views here.


class JobSeekerHomeView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerhome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['joblist'] = Job.objects.all()
        return context


class JobSeekerRegistrationView(CreateView):
    template_name = 'jobseekertemplates/jobseekerregistration.html'
    form_class = JobSeekerForm
    success_url = '/'

    def form_valid(self, form):  # to create a user when jobseeker registered
        u_name = form.cleaned_data['username']
        pword = form.cleaned_data['password']

        # create_user for storing data along with protection
        user = User.objects.create_user(u_name, '', pword)

        form.instance.user = user

        # userle register hunebittikai login garne
        login(self.request, user)  # import login to use login function
        return super().form_valid(form)


class JobListView(ListView):
    template_name = 'jobseekertemplates/joblist.html'
    queryset = Job.objects.all()
    context_object_name = 'joblist'


class JobDetailView(DetailView):
    template_name = 'jobseekertemplates/jobdetail.html'
    model = Job
    context_object_name = 'jobdetail'


# apply is done after login by the jobseeker only...not login by admin but by jobseeker....we can defined a Custom Login required mixin
class JobSeekerJobApplyView(CreateView):

    template_name = 'jobseekertemplates/jobseekerjobapply.html'
    form_class = JobApplyForm  # forms should be defined at forms.py of name JobApplyForm
    success_url = '/'

    # job seeker form bata cover letter matra linxa
    # kun job ra kasle apply gareko manage garna form bata form valid method bata manage garne

    # to contro request and response for applying and it is also similar as LoginRequiredMixin

    def dispatch(self, request, *args, **kwargs):
        print(request.user.groups, "******************")

        if request.user.is_authenticated and request.user.groups.filter(name="jobseeker").exists():
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        job_id = self.kwargs['pk']  # id gets from this
        job = Job.objects.get(id=job_id)  # id vayeko job

        user = self.request.user  # for logged_in user,currently logged in user
        job_seeker = JobSeeker.objects.get(
            user=user)  # jobseeker with user Jobseeker
        # form ma 3 field hunxa JobApplicationModel ko jasko job field ma chai hamile mathi job bata job id liyera job halxauu....
        form.instance.job = job
        form.instance.jobseeker = job_seeker

        return super().form_valid(form)


class LoginView(FormView):
    template_name = 'jobseekertemplates/jobseekerlogin.html'
    form_class = LoginForm
    success_url = '/'

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']

        user = authenticate(username=uname, password=pword)
        # variable of the fuction is made the attribute of the class
        self.thisuser = user  # self is used to access class variable into fuction,we can make function variable into class variable using self
        # if user is not None: if user is superuser with no any Groups then SU can login but adding {and user.groups.exist():} in condition SU can't get logged in
        if user is not None and user.groups.exists():
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                'error': 'your username doesnot exist',
                'form': form  # to show login form when incorrect login
            })
        return super().form_valid(form)


# to override the success url we use get_success_url

    def get_success_url(self):

        user = self.thisuser
        # return ('/')
        # return reverse('jobapp':jobseekerhome),import reverse
        # if user.groups.first().name == 'jobseeker':

        if user.groups.filter(name='jobseeker').exists():
            return reverse('jobapp:jobseekerhome')

        elif user.groups.filter(name='employer').exists():
            return reverse('jobapp:employerhome')

        else:
            return reverse('jobapp:login')


class JobSeekerProfileView(TemplateView):
    template_name = 'jobseekertemplates/jobseekerprofile.html'

    # this first method helps to login the user to its profile

    def dispatch(self, request, *args, **kwargs):
        user = request.user  # to take user
        if user.is_authenticated and user.groups.first().name == 'jobseeker':
            pass
        else:
            return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)

    # def dispatch(self, request, *args, **kwargs):
    #     user = request.user  # to take user
    #     if user.is_authenticated and user.groups.first().name == 'jobseeker':
    #         pass
    #     else:
    #         return redirect('/login/')

        return super().dispatch(request, *args, **kwargs)
    # this second method helps to send response to html page(In get context data, super is call at 2nd but it others super() is at last)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        jobseeker = JobSeeker.objects.get(user=logged_user)
        context['jobseeker'] = jobseeker  # sends value through key 'jobseeker'
        return context


# EMPLOYER SECTION

class EmployerRegistrationView(CreateView):
    template_name = 'employertemplates/employerregistration.html'
    form_class = EmployerForm
    success_url = '/'

    # if successfully validated at cleaned method
    # in the form's content stored at forms.py at EmployerForm

    def form_valid(self, form):
        uname = form.cleaned_data['username']
        pword = form.cleaned_data['password']

        # user created at django user
    # form bata add vayeko input bata user create gardeko i.e registration gareko vaneko user create gareko
        user = User.objects.create_user(uname, '', pword)

        form.instance.user = user  # models ma user bahek ko kura forms ma xa, but, user lai hamle username ra password validate garayera matra form ko instance ma jodinxau so that all models ko field fullfill hunxa ra Integrity error pani hatauxa

        return super().form_valid(form)


class EmployerHomeView(EmployerRequiredMixin  , TemplateView):
    template_name = 'employertemplates/employerhome.html'

# to give render to only the authorized user using dispatch method

    # def dispatch(self, request, *args, **kwargs):

    #     usr = request.user
    #     if usr.is_authenticated and usr.groups.filter(name='employer').exists():
    #         pass
    #     else:
    #         return redirect('/login/')

    #     return super().dispatch(request, *args, **kwargs)


class EmployerProfileView(EmployerRequiredMixin, TemplateView):
    template_name = 'employertemplates/employerprofile.html'

    # def dispatch(self, request, *args, **kwargs):
    #     user = request.user  # to take user currently logged in
    #     if user.is_authenticated and user.groups.filter(name='employer').exists():
    #         pass
    #     else:
    #         return redirect('/login/')  # rediect("jobapp:login")

    #     return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        logged_user = self.request.user
        # employer = Employer.objects.get(user=self.request.user)
        employer = Employer.objects.get(user=logged_user)
        context['employer'] = employer

        return context


class EmployerJobDetailView(DetailView):
    template_name = 'employertemplates/employerjobdetail.html'
    model = Job
    context_object_name = 'jobobject'


class EmployerJobCreateView(EmployerRequiredMixin, CreateView):
    template_name = 'employertemplates/employerjobcreate.html'
    form_class = EmployerJobPostForm
    success_url = reverse_lazy('jobapp:employerprofile')

    # Only the logged_in user can post the job.So to guarentee the employer
    # we need to check it using dispatch method

    # def dispatch(self, request, *args, **kwargs):
    #     usr = request.user
    #     if usr.is_authenticated and usr.groups.filter(name='employer').exists():
    #         pass
    #     else:
    #         return redirect('jobapp:login')

    #     return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):

        employer = Employer.objects.get(user=self.request.user)
        form.instance.employer = employer

        return super().form_valid(form)
