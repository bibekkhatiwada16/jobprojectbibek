from django.db import models
from django.contrib.auth.models import User, Group


# Create your models here.

# timestamp class which doesnot need object also called as abstract class


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Admin(TimeStamp):
    # extend django mdels of user using onetoone
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    address = models.CharField(max_length=200)
    image = models.ImageField(upload_to='admin')
    name = models.CharField(max_length=200)

    def __str__(self):

        return self.name


class Employer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=100)
    email = models.EmailField()
    address = models.CharField(max_length=200)
    image = models.ImageField(upload_to='employer')
    company = models.CharField(max_length=200)
    website = models.CharField(max_length=200)
    company_image = models.ImageField(upload_to='employer')


    def save(self, *args, **kwargs):  # data save hudahudai a bit customize garera group ma haldina lai...model is a bit changed    
        group, created = Group.objects.get_or_create(name='employer')
        self.user.groups.add(group)
        super().save(*args, **kwargs)

    def __str__(self):

        return self.name

class JobSeeker(TimeStamp):
    # foriegn key with user model of django
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    mobile = models.CharField(max_length=100)
    image = models.ImageField(upload_to='jobseeker')
    qualification = models.CharField(max_length=100)
    skills = models.CharField(max_length=100)
    about = models.CharField(max_length=500)
    cv = models.FileField(upload_to='jobseeker')

    def __str__(self):

        return self.name

    def save(self, *args, **kwargs):
        group, created = Group.objects.get_or_create(name='jobseeker')

        self.user.groups.add(group)  # userko group ma mathi ko group add

        super().save(*args, **kwargs)


class JobCatagory(TimeStamp):
    title = models.CharField(max_length=200)
    image = models.ImageField(upload_to='jobseeker')

    def __str__(self):
        return self.title


JOB_TYPE = (
    ('full_time', 'FULL TIME'),
    ('part_time', 'PART TIME'),
    ('contract', 'Contarct'),
    ('internship', 'Internship'),

)

LEVEL = (
    ('entry', 'Entry Level'),
    ('jr_level', 'Junior Level'),
    ('mid_level', 'Mid Level'),
    ('sr_level', 'Senior Level'),

)


class Job(TimeStamp):
    title = models.CharField(max_length=200)
    catagory = models.ForeignKey(JobCatagory, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='job')
    job_type = models.CharField(
        max_length=50, choices=JOB_TYPE)  # to used dropdown box
    # foreign key; many job=one employer,,,one emplohyer can post many jobs
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE)
    salary = models.CharField(max_length=100)
    level = models.CharField(max_length=50, choices=LEVEL)
    deadline = models.DateTimeField()
    vacancy_number = models.PositiveIntegerField()
    educations = models.CharField(max_length=200)
    skills = models.TextField(max_length=200)
    details = models.TextField()
    views_count = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.title


class JobApplication(TimeStamp):
    # which job to be applied
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    jobseeker = models.ForeignKey(JobSeeker, on_delete=models.CASCADE)
    cover_letter = models.FileField(upload_to='jobapplication')

    def __str__(self):
        return self.jobseeker.name
